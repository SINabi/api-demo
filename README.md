Introduction

This program is developed to validate the three acceptance criteria using the given API.

Installation

As a pre-requisite we need to install below listed Jar files and plug in to run the program successfully:

•	Java development kit
•	Rest Assure
•	TestNG
•	Eclipse

User Guide

We have three validation points as per acceptance criteria: 
Case 1.
We need to verify the field “Name” and that “Name” should be “Carbon Credits”.

Case 2.
We need to verify the field “CanRelist” in response and the “CanRelist” value must be “true”.

Case 3.
We need to find the test “Gallery” that should has a description that contains the text “2x larger image”.

Implementation

To run the code we need to open Eclipse and import the project from saved location to run again and validate the expected result.

Testing

Scope of testing was to validate the existence of three acceptance criteria that is meet successfully as expected. Using TestNG we can check pass status after code run.

Related URLs

Download Eclipse:  https://www.eclipse.org/downloads/
Download Java: https://www.oracle.com/technetwork/java/javase/downloads/index.html
Download TentNg: https://www.toolsqa.com/selenium-webdriver/install-testng/
API = https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false
